var express = require('express')
var app = express()

var Firebase = require("firebase");

app.set('view engine', 'jade');

app.get('/', function (req, res) {
  res.render('index');
})

app.get('/:name', function (req, res) {
  res.render(req.params.name);
})

app.get('/css/:file', function (req, res) {
	res.sendFile(__dirname + '/css/' + req.params.file)
})

var server = app.listen(3000, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('Example app listening at http://%s:%s', host, port)

})
